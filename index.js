// Repetition Control Structures

// While Loop
/* Syntax:
	while(condition){
		statement/s;
	}
*/

let count = 5;

// count = 0
while(count !== 0){
	console.log("While: " + count);
	count--;
}

console.log("Displays numbers 1-10")
count = 1;

while(count < 11){
	console.log("While: " + count);
	count++;
}

// Do While Loop
/* Syntax:
	do{
		statement;
	}while (condition);
*/

// Number() is similar to parseInt() when converting string into numbers
/*let number = Number(prompt("Give me a number"));

do{
	console.log("Do While: " + number);
	number += 1;
}while (number < 10);*/

console.log("Diplays even numbers from 2-10 using while loop")
let num = 2
while(num <= 10){
	console.log("Even: " + num);
	num += 2;
}

console.log("Diplays even numbers from 2-10 using do while loop")
let numA = 2

do{
	console.log("Even: " + numA);
	numA += 2;
}while (numA <= 10);

// For Loop
/* Syntax:
	for(initialization;condition;stepExpression){
		statement;
	}
*/

console.log("For Loop");

for(let count = 0;count <= 20; count++){
	console.log(count);
}

console.log("Even For Loop");
let even = 2
for(let counter = 1; counter <= 5; counter++){
	console.log("Even: " + even);
	even += 2;
}

console.log("Other for loop examples:")
let myString = 'alex';
// .length property is used to count the characters in a string
console.log(myString.length);

console.log(myString[0]);

// myString.length = 4
// x = 0, 1, 2, 3

for(let x = 0; x < myString.length; x++){
	console.log(myString[x]);
}
/* Expected output:
	a
	l
	e
	x
*/

 for (let x = myString.length - 1 ; x >= 0; x--) {
	console.log(myString[x]);
}

// Print out lettes individually, but will print 3 instead of the vowels
let myName = "AlEx";

for(let i = 0; i < myName.length; i++){
	if(
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u" 
	){
		console.log(3);
	}
	else{
		console.log(myName[i]);
	}
}

// Continue and Break Statements

for(let count = 0; count <= 20; count++){
	// if remainder is equal to 0, tells the code to continue to iterate
	if(count % 2 === 0){
		continue;
	}
	console.log('Continue and Break: ' + count);

	// if the current value of count is greater than 10, tells the code to stop the loop
	if(count > 10){
		break;
	}
}

let name = 'alexandro';

for(let i = 0; i < name.length; i++){
	console.log(name[i]);
	
	if(name[i].toLowerCase() === "a"){
		console.log("Continue to the next iteration");
		continue;
	}

	if(name[i] === "d"){
		break;
	}
}